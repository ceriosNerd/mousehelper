//
//  main.cpp
//  MouseHelper
//
//  Created by Andrew Querol on 1/12/15.
//  Copyright (c) 2015 Andrew Querol. All rights reserved.
//

#include <iostream>
#include <ApplicationServices/ApplicationServices.h>

typedef enum : unsigned char {
    BackButton = 3,
    ForwardButton = 4,
} MouseButtons;

typedef enum :unsigned char {
    CloseBracket = 30,
    OpenBracket = 33,
} KeyboardKeyCodes;

static void sendKeyMacro(MouseButtons button) {
    CGKeyCode keyCode = 0;
    switch (button) {
        case BackButton:
            keyCode = OpenBracket;
            break;
        case ForwardButton:
            keyCode = CloseBracket;
            break;
        default:
            break;
    }
    
    CGEventSourceRef source = CGEventSourceCreate(kCGEventSourceStateCombinedSessionState);
    CGEventRef saveCommandDown = CGEventCreateKeyboardEvent(source, keyCode, true);
    CGEventSetFlags(saveCommandDown, kCGEventFlagMaskCommand);
    CGEventRef saveCommandUp = CGEventCreateKeyboardEvent(source, keyCode, false);
    
    CGEventPost(kCGAnnotatedSessionEventTap, saveCommandDown);
    CGEventPost(kCGAnnotatedSessionEventTap, saveCommandUp);
    
    CFRelease(saveCommandUp);
    CFRelease(saveCommandDown);
    CFRelease(source);
}

CGEventRef myCGEventCallback(CGEventTapProxy proxy, CGEventType type, CGEventRef event, void *refcon) {
    if (type != kCGEventOtherMouseUp)
        return event;
    
    // The incoming mouse button number.
    CGKeyCode keycode = (CGKeyCode)CGEventGetIntegerValueField(event, kCGMouseEventButtonNumber);
    
    /*
     * We only want the extra buttons. The first three (0, 1, 2) arge Left, Right, and Center respectfully
     */
    switch (keycode) {
        case BackButton:
            sendKeyMacro(BackButton);
            break;
        case ForwardButton:
            sendKeyMacro(ForwardButton);
            break;
        default:
            break;
    }
    
    // Return the event, if we don't we would cancel the event.
    return event;
}

int main(int argc, char** argv) {
    CFMachPortRef      eventTap;
CGEventMask        eventMask;
    CFRunLoopSourceRef runLoopSource;
    
    // Create an event tap. We are interested in mouse button presses.
    eventMask = 1 << kCGEventOtherMouseUp;
    eventTap = CGEventTapCreate(kCGSessionEventTap, kCGHeadInsertEventTap, 0, eventMask, myCGEventCallback, NULL);
    if (!eventTap) {
        std::cout<<"MouseHelper: Failed creating event tap for the mouse button presses.";
        return EXIT_FAILURE;
    }
    
    // Create a run loop source.
    runLoopSource = CFMachPortCreateRunLoopSource(kCFAllocatorDefault, eventTap, 0);
    
    // Add to the current run loop.
    CFRunLoopAddSource(CFRunLoopGetCurrent(), runLoopSource, kCFRunLoopCommonModes);
    
    // Enable the event tap.
    CGEventTapEnable(eventTap, true);
    
    // Set it all running.
    CFRunLoopRun();
    
    return EXIT_SUCCESS;
}